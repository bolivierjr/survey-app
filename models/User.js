const mongoose = require('mongoose');
// const Schema = mongoose.Schema; Next line is es2015 destructuring
const { Schema } = mongoose;

const userSchema = new Schema({
  googleId: String,
  credits: { type: Number, default: 0 }
});

mongoose.model('users', userSchema);
